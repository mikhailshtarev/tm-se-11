//package org.shtarev.tmse11.commands;
//
//import org.jetbrains.annotations.NotNull;
//import org.shtarev.tmse11.entity.User;
//
//import java.io.FileOutputStream;
//import java.io.ObjectOutputStream;
//import java.util.List;
//
//public class UserSerializationWrite1 extends AbstractCommand {
//    @Override
//    public String getName() {
//        return "UserSerializationWrite1";
//    }
//
//    @Override
//    public String getDescription() {
//        return "Write Predmet oblast' with serializathion";
//    }
//
//    @Override
//    public @NotNull UserRole[] getListRole() {
//        return UserRole.values();
//    }
//
//
//    @Override
//    public void execute() throws Exception {
//        List<User> listUser = serviceLocator.getUserService().getUserList();
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("user.txt"));
//        objectOutputStream.writeObject(listUser);
//    }
//}
