package org.shtarev.tmse11.commands;

import org.jetbrains.annotations.NotNull;

final public class UserRePassword extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserRePassword";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update you password";
    }

    @Override
    public void execute() {
        System.out.println("Введите имя пользователя, у которого хотите поменять пароль: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите старый пароль: ");
        @NotNull final String oldPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новый пароль: ");
        @NotNull final String newPassword = serviceLocator.getTerminalService().nextLine();
        if (serviceLocator.getSession().getHashBox().isEmpty()){
            System.out.println("Сначала войдите в аккаунт");
        return;}
        serviceLocator.getClientUserService().rePassword(name, oldPassword, newPassword, serviceLocator.getSession());
    }
}
