package org.shtarev.tmse11.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.controller.Session;

final public class UserLogIn extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserLogIn";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sign in";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите имя пользователя: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль: ");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        Session session = serviceLocator.getClientUserService().LogIn(name, password);
        session.setHashBox(null);
        serviceLocator.setSession(session);
        serviceLocator.setUserRole(serviceLocator.getClientUserService().checkUserRole(session));
    }
}
