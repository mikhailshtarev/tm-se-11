package org.shtarev.tmse11.commands;

public abstract class AbstractCommand {


    protected ServiceLocator serviceLocator;


    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;
}
