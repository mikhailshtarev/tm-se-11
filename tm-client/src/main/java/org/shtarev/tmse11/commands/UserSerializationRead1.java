//package org.shtarev.tmse11.commands;
//
//import org.jetbrains.annotations.NotNull;
//import org.shtarev.tmse11.entity.User;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.ObjectInputStream;
//import java.util.List;
//
//public class UserSerializationRead1 extends AbstractCommand {
//    @Override
//    public String getName() {
//        return "UserSerializationRead1";
//    }
//
//    @Override
//    public String getDescription() {
//        return "Read Predmet oblast' with serializathion";
//    }
//
//    @Override
//    public @NotNull UserRole[] getListRole() {
//        return UserRole.values();
//    }
//
//    @Override
//    public void execute() throws Exception, IOException, ClassNotFoundException {
//
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("user.txt"));
//        List<User> listRole = (List<User>) ois.readObject();
//        System.out.println(listRole);
//    }
//}
