//package org.shtarev.tmse11.commands;
//
//import org.jetbrains.annotations.NotNull;
//import org.shtarev.tmse11.entity.UserStore;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.Unmarshaller;
//import java.io.File;
//import java.io.IOException;
//
//public class UserJaxbXMLRead2 extends AbstractCommand {
//    @Override
//    public String getName() {
//        return "UserJaxbXMLRead2";
//    }
//
//    @Override
//    public String getDescription() {
//        return "Read Predmet oblast' with Externalizathion";
//    }
//
//
//    @Override
//    public @NotNull UserRole[] getListRole() {
//        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
//    }
//
//    private static final String USERSTORE_XML = "src/main/resources/userlist.xml";
//
//    @Override
//    public void execute() throws Exception, IOException, ClassNotFoundException {
//        JAXBContext context = JAXBContext.newInstance(UserStore.class);
//        Unmarshaller un = context.createUnmarshaller();
//        UserStore unmarshUser = (UserStore) un.unmarshal(new File(USERSTORE_XML));
//        System.out.println(unmarshUser.toString());
//    }
//}
