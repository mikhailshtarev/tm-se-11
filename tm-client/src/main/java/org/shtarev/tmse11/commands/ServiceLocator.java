package org.shtarev.tmse11.commands;

import org.shtarev.tmse11.controller.Session;
import org.shtarev.tmse11.controller.UserRole;
import org.shtarev.tmse11.service.ClientProjectService;
import org.shtarev.tmse11.service.ClientTaskService;
import org.shtarev.tmse11.service.ClientUserService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    ClientProjectService getClientProjectService();

    ClientTaskService getClientTaskService();

    ClientUserService getClientUserService();

    List<AbstractCommand> getCommands();

    Session getSession();

    void setSession(Session session);

    Scanner getTerminalService();

    void setUserRole(List<UserRole> userRole);
}
