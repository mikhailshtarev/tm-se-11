package org.shtarev.tmse11.commands;

import org.jetbrains.annotations.NotNull;

final public class TaskClearCommand extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "TaskClear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all Tasks";
    }

    @Override
    public void execute() {
        serviceLocator.getClientTaskService().taskDeleteAllCommand(serviceLocator.getSession());
    }
}
