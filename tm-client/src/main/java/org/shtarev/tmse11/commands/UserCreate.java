package org.shtarev.tmse11.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.controller.User;

final public class UserCreate extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "UserCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create New User";
    }


    @Override
    public void execute() {
        System.out.println("Введите имя нового пользователя: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль для этого пользователя: ");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        User thisUser = new User();
        thisUser.setName(name);
        thisUser.setPassword(password);
        serviceLocator.getClientUserService().create(thisUser);
    }
}
