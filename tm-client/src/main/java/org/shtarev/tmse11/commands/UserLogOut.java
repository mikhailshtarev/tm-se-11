package org.shtarev.tmse11.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.controller.Session;
import org.shtarev.tmse11.controller.User;

final public class UserLogOut extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserLogout";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User ends the session";
    }

    @Override
    public void execute() {
        Session session = new Session();
       serviceLocator.setSession(session);
       serviceLocator.setUserRole(null);
    }
}
