package org.shtarev.tmse11.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.controller.Project;
import org.shtarev.tmse11.controller.Session;
import org.shtarev.tmse11.controller.TaskProjectStatus;

import java.util.List;

public interface ClientProjectService {

    boolean projectCreate(@NotNull String projectName, @NotNull String description,
                       @NotNull String dateStartSP, @NotNull String dateFinishSP,
                       @NotNull Session session);

    boolean projectClear(Session session);

    List<Project> projectReadAll(@NotNull Session session);

    boolean projectDeleteById(@NotNull String projectId, @NotNull Session session);

    List<Project> projectsSort(@NotNull String sort, @NotNull Session session);

    List<Project> projectFindByDescription(@NotNull String partName, @NotNull Session session);

    boolean projectUpdate(@NotNull String projectId, @NotNull Project project,@NotNull String taskProjectStatus, @NotNull Session session);
}
