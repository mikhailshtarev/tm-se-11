package org.shtarev.tmse11.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.controller.Session;
import org.shtarev.tmse11.controller.Task;

import java.util.List;

public interface ClientTaskService {
    boolean taskCreate(@NotNull Task task, @NotNull Session session);

    List<Task> taskFindByDescription(@NotNull String partName, @NotNull Session session);

    boolean taskDeleteAllCommand(@NotNull Session session);

    @NotNull List<Task> taskList(@NotNull Session session);

    boolean taskDeleteOne(@NotNull String taskId, @NotNull Session session);


    boolean taskUpdate(@NotNull String taskId, @NotNull Task task, @NotNull String taskPS, @NotNull Session session);
}
