package org.shtarev.tmse11.service;


import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.controller.Session;
import org.shtarev.tmse11.controller.User;
import org.shtarev.tmse11.controller.UserRole;

import java.util.List;

public interface ClientUserService {


    boolean create(User user);

    List<User> getUserList(@NotNull Session session);

    Session LogIn(String name, String password) throws Exception;

    boolean rePassword(String name, String oldPassword, String newPassword,Session session);

    boolean userUpdate(String name, Session session);

    List<UserRole> checkUserRole(Session session);
}
