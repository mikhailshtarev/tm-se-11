package org.shtarev.tmse11.BD;

public class Const {
    public static final String USER_TABLE="users";
    public static final String USER_ID="idusers";
    public static final String USER_NAME="firstname";
    public static final String USER_PASSWORD="password";
    public static final String USER_ROLE="userrole";
}
