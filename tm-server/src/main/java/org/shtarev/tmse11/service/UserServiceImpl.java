package org.shtarev.tmse11.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse11.commands.UserRole;
import org.shtarev.tmse11.entity.Session;
import org.shtarev.tmse11.entity.User;
import org.shtarev.tmse11.repository.SessionRepository;
import org.shtarev.tmse11.repository.UserRepository;

import java.util.List;

import static org.shtarev.tmse11.commands.UserRole.ADMIN;
import static org.shtarev.tmse11.commands.UserRole.REGULAR_USER;

@Slf4j
public class UserServiceImpl implements UserService<User> {

    @NotNull
    private final UserRepository<User> userRepository;


    @NotNull
    private final SessionUtil sessionUtil;

    @NotNull
    public UserServiceImpl(@NotNull final UserRepository<User> userRepository,@NotNull final SessionUtil sessionUtil) {
        this.userRepository = userRepository;
        this.sessionUtil = sessionUtil;
    }

    public boolean create(@NotNull final User user) {
        if (user.getName() == null || user.getUserRole() == null || user.getPassword() == null)
            return false;
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        return userRepository.create(user);
    }

//    @Override
//    @Nullable
//    public Session LogIn(@NotNull final String name, @NotNull final String password) {
//        @NotNull final User thisUser = userRepository.getUser(name);
//        if (thisUser == null)
//            return null;
//        @NotNull final String thisPassword = DigestUtils.md5Hex(password);
//        if (name.equals(thisUser.getName()) && thisPassword.equals(thisUser.getPassword())) {
//            return sessionUtil.createSession(thisUser.getName());
//        }
//        return null;
//    }
//
//    @Nullable
//    public List<User> getUserList(@NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if ((user.getUserRole()).contains(ADMIN)) {
//                return userRepository.getUserList();
//
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public boolean rePassword(@NotNull String name, @NotNull String oldPassword, @NotNull String newPassword,
//                              @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            if (name.isEmpty() || oldPassword.isEmpty() || newPassword.isEmpty()) {
//                return false;
//            }
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER)) {
//                String oldPassword2 = DigestUtils.md5Hex(oldPassword);
//                return userRepository.rePassword(name, oldPassword2, newPassword);
//            }
//
//        }
//        return false;
//    }
//
//    @Override
//    public boolean userUpdate(@NotNull final String name, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            if (name.isEmpty() || session.getName().isEmpty()) {
//                return false;
//            }
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER)) {
//                return userRepository.updateUser(name, session.getName());
//            }
//        }
//        return false;
//    }
//
//
//    @Override
//    public List<UserRole> getListUserRole(Session session) {
//        if (sessionUtil.checkSession(session)) {
//            return userRepository.getListUserRole(session.getName());
//        }
//        return null;
//    }
}


