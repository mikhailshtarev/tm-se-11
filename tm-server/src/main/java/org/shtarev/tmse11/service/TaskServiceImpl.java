//package org.shtarev.tmse11.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.shtarev.tmse11.commands.TaskProjectStatus;
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.entity.Task;
//import org.shtarev.tmse11.entity.User;
//import org.shtarev.tmse11.repository.SessionRepository;
//import org.shtarev.tmse11.repository.TaskRepository;
//import org.shtarev.tmse11.repository.UserRepository;
//import org.shtarev.tmse11.util.SignatureUtil;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import static org.shtarev.tmse11.commands.UserRole.ADMIN;
//import static org.shtarev.tmse11.commands.UserRole.REGULAR_USER;
//
//public class TaskServiceImpl implements TaskService<Task> {
//    @NotNull
//    private final TaskRepository<Task> taskRepository;
//
//    @NotNull
//    private final UserRepository<User> userRepository;
//
//    @NotNull
//    private final SessionUtil sessionUtil;
//
//    @NotNull
//    public TaskServiceImpl(final @NotNull TaskRepository<Task> taskRepository, @NotNull final UserRepository<User> userRepository,
//                           @NotNull final SessionUtil sessionUtil) {
//        this.taskRepository = taskRepository;
//        this.userRepository = userRepository;
//        this.sessionUtil = sessionUtil;
//
//    }
//
//    @Override
//    public boolean create(@NotNull final Task task, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                task.setUserId(userRepository.getUser(session.getName()).getUserId());
//                return taskRepository.create(task);
//            }
//        }
//        return false;
//    }
//
//
//    @Override
//    public boolean update(@NotNull final String taskId, @NotNull final Task task, @NotNull final String taskPS,
//                          @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                if (taskPS.equals("IN_THE_PROCESS"))
//                    task.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.IN_THE_PROCESS});
//                if (taskPS.equals("READY"))
//                    task.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.READY});
//                return taskRepository.update(taskId, task);
//            }
//        }
//        return false;
//    }
//
//    @Override
//    public ArrayList<Task> findByDescription(@NotNull final String partName, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                @NotNull final Collection<Task> taskList = taskRepository.getTaskMap().values();
//                ArrayList<Task> taskList2 = new ArrayList<>();
//                taskList.forEach(task -> {
//                    if ((task.getName().contains(partName)) || (task.getDescription().contains(partName)))
//                        taskList2.add(task);
//                });
//                return taskList2;
//            }
//        }
//        return null;
//    }
//
//
//    @Override
//    public boolean delete(@NotNull final String id, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            if (id.isEmpty()) {
//                return false;
//            }
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                    taskRepository.remove(id, session.getName());
//                }
//            }
//        return false;
//        }
//
//
//        @Override
//        @Nullable
//        public List<Task> taskReadAll ( @NotNull final Session session){
//            if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                        return taskRepository.findAll(session.getName());
//                    }
//                }
//                return null;
//        }
//
//        @Override
//        public boolean taskDeleteAll ( @NotNull final Session session){
//            if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                        taskRepository.removeAll(session.getName());
//                        return true;
//                    }
//                }
//            return false;
//            }
//    }
//
//
//
//
//
