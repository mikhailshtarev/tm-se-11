//package org.shtarev.tmse11.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.shtarev.tmse11.Comparator.ProjecDataCreateComparetor;
//import org.shtarev.tmse11.Comparator.ProjectDataFinishComparator;
//import org.shtarev.tmse11.Comparator.ProjectDateStartComparator;
//import org.shtarev.tmse11.Comparator.ProjectStatusComparator;
//import org.shtarev.tmse11.commands.TaskProjectStatus;
//import org.shtarev.tmse11.entity.Project;
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.entity.Task;
//import org.shtarev.tmse11.entity.User;
//import org.shtarev.tmse11.repository.ProjectRepository;
//import org.shtarev.tmse11.repository.TaskRepository;
//import org.shtarev.tmse11.repository.UserRepository;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
//import static org.shtarev.tmse11.commands.UserRole.ADMIN;
//import static org.shtarev.tmse11.commands.UserRole.REGULAR_USER;
//
//
//public class ProjectServiceImpl implements ProjectService<Project> {
//    @NotNull
//    private final ProjectRepository<Project> projectRepository;
//    @NotNull
//    private final TaskRepository<Task> taskRepository;
//    @NotNull
//    private final UserRepository<User> userRepository;
//    @NotNull
//    private final SessionUtil sessionUtil;
//
//    @NotNull
//    public ProjectServiceImpl(@NotNull ProjectRepository<Project> projectRepository, @NotNull TaskRepository<Task> taskRepository,
//                              @NotNull UserRepository<User> userRepository,
//                              @NotNull SessionUtil sessionUtil) {
//        this.projectRepository = projectRepository;
//        this.taskRepository = taskRepository;
//        this.userRepository = userRepository;
//        this.sessionUtil = sessionUtil;
//    }
//
//    @Override
//    public boolean create(@NotNull final Project project, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                project.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.PLANNED});
//                project.setUserId(userRepository.getUser(session.getName()).getUserId());
//                return projectRepository.create(project);
//            }
//        }
//        return false;
//    }
//
//    @Override
//    public boolean update(@NotNull final String projectId, @NotNull final Project project, @NotNull final String taskProjectStatus, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                if (taskProjectStatus.equals("IN_THE_PROCESS"))
//                    project.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.IN_THE_PROCESS});
//                if (taskProjectStatus.equals("READY"))
//                    project.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.READY});
//                return projectRepository.update(projectId, project);
//
//            }
//        }
//        return false;
//    }
//
//
//    @Override
//    public TreeSet<Project> sorted(@NotNull final String sort, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER)) {
//                switch (sort) {
//                    case "1":
//                        TreeSet<Project> projects = new TreeSet<>(new ProjectDateStartComparator());
//                        projects.addAll(projectRepository.getProjectMap((userRepository.getUser(session.getName()).getUserId())));
//                        return projects;
//                    case "2":
//                        TreeSet<Project> projects1 = new TreeSet<>(new ProjectDataFinishComparator());
//                        projects1.addAll(projectRepository.getProjectMap((userRepository.getUser(session.getName()).getUserId())));
//                        return projects1;
//                    case "3":
//                        TreeSet<Project> projects2 = new TreeSet<>(new ProjecDataCreateComparetor());
//                        projects2.addAll(projectRepository.getProjectMap((userRepository.getUser(session.getName()).getUserId())));
//                        return projects2;
//                    case "4":
//                        TreeSet<Project> projects3 = new TreeSet<>(new ProjectStatusComparator());
//                        projects3.addAll(projectRepository.getProjectMap((userRepository.getUser(session.getName()).getUserId())));
//                        return projects3;
//                    default:
//                        return null;
//                }
//
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public ArrayList<Project> findByDescription(@NotNull final String partName, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER)) {
//                @NotNull final Collection<Project> projectList = projectRepository.getProjectMap((userRepository.getUser(session.getName()).getUserId()));
//                ArrayList<Project> projectList2 = new ArrayList<>();
//                projectList.forEach(project -> {
//                    if ((project.getName().contains(partName)) || (project.getDescription().contains(partName)))
//                        projectList2.add(project);
//                });
//                return projectList2;
//            }
//        }
//        return null;
//    }
//
//
//    @Override
//    public boolean delete(@NotNull final String id, @NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            if (id.isEmpty()) {
//                return false;
//            }
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                projectRepository.remove(id, session.getName());
//                taskRepository.remove(id, session.getName());
//                return true;
//            }
//        }
//        return false;
//    }
//
//    @Override
//    @Nullable
//    public List<Project> projectsReadAll(@NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                return projectRepository.getProjectMap(userRepository.getUser(session.getName()).getUserId());
//            }
//        }
//        return null;
//    }
//
//
//    @Override
//    public boolean projectDeleteAll(@NotNull final Session session) {
//        if (sessionUtil.checkSession(session)) {
//            @NotNull final User user = userRepository.getUser(session.getName());
//            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
//                projectRepository.removeAll(session.getName());
//                taskRepository.removeAll(session.getName());
//                return true;
//            }
//        }
//        return false;
//    }
//}
//
