package org.shtarev.tmse11.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.commands.UserRole;
import org.shtarev.tmse11.entity.Session;
import org.shtarev.tmse11.entity.User;

import java.util.List;

public interface UserService<U extends User> {

    boolean create(U user);

//    List<U> getUserList(Session session);
//
//    Session LogIn(String name, String password) throws Exception;
//
//    boolean rePassword(String name, String oldPassword, String newPassword, Session session);
//
//    boolean userUpdate(String name, Session session);
//
//    List<UserRole> getListUserRole(Session session);
}