//package org.shtarev.tmse11.service;
//
//import org.shtarev.tmse11.entity.Project;
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.entity.Task;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//public interface ProjectService<T extends Project> {
//
//
//    boolean create(Project project, Session session);
//
//    boolean update(String projectId, Project project,String taskProjectStatus, Session session);
//
//    Set<T> sorted(String sort, Session session);
//
//    ArrayList<T> findByDescription(String partName, Session session);
//
//    boolean delete(String id,  Session session);
//
//    List<T> projectsReadAll( Session session);
//
//    boolean projectDeleteAll( Session session);
//}
