package org.shtarev.tmse11.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.entity.Session;
import org.shtarev.tmse11.repository.SessionRepository;
import org.shtarev.tmse11.util.HashProperties;
import org.shtarev.tmse11.util.SignatureUtil;

public class SessionUtilImpl implements SessionUtil {

    private final SessionRepository sessionRepository;

    public SessionUtilImpl(@NotNull final SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public Session createSession(@NotNull final String userName) {
        Session session = new Session();
        session.setName(userName);
        session.setHashBox(SignatureUtil.sign(session, HashProperties.getSalt(), HashProperties.getCycol()));
        if (sessionRepository.createSes(session))
            return session;
        return null;
    }

    public boolean checkSession(@NotNull final Session session) {
        Session session1 = sessionRepository.checkSession(session.getId());
        session.setHashBox(SignatureUtil.sign(session, HashProperties.getSalt(), HashProperties.getCycol()));
        return session1.getHashBox().equals(session.getHashBox());
    }
}
