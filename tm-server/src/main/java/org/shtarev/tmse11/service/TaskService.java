//package org.shtarev.tmse11.service;
//
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.entity.Task;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public interface TaskService<T extends Task> {
//
//    boolean create(T task, Session session);
//
//    boolean update(String taskId,T task,String taskPS,Session session);
//
//    ArrayList<T> findByDescription(String partName,Session session);
//
//    boolean delete(String id,Session session);
//
//    List<T> taskReadAll(Session session);
//
//    boolean taskDeleteAll(Session session);
//}
