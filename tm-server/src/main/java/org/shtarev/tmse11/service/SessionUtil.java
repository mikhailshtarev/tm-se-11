package org.shtarev.tmse11.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.entity.Session;

public interface SessionUtil {
  Session createSession(@NotNull String UserName);

 boolean checkSession(@NotNull Session session);
}
