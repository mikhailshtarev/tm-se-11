package org.shtarev.tmse11.Comparator;

import org.shtarev.tmse11.entity.Project;

import java.util.Comparator;

public class ProjectDataFinishComparator implements Comparator<Project> {

    public int compare(Project a, Project b) {
        assert a.getDataFinish() != null;
        assert b.getDataFinish() != null;
        int i = a.getDataFinish().compareTo(b.getDataFinish());
        if (i != 0) return i;
        else return 1;
    }
}
