package org.shtarev.tmse11.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse11.entity.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class TaskRepositoryImpl implements TaskRepository<Task> {
    @Nullable
    private final Map<String, Task> taskMap = new HashMap<>();

    @Override
    public boolean create(@NotNull final Task thisTask) {
        if (taskMap != null && !taskMap.containsValue(thisTask)) {
            taskMap.put(thisTask.getId(), thisTask);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(@NotNull final String taskId, @NotNull final Task thisTask) {
        if (taskMap != null && taskMap.get(taskId) != null) {
            Task task = taskMap.get(taskId);
            task.setUserId(thisTask.getUserId());
            task.setDataStart(thisTask.getDataStart());
            task.setDataFinish(thisTask.getDataFinish());
            task.setDescription(thisTask.getDescription());
            task.setTaskProjectStatus(thisTask.getTaskProjectStatus());
            return true;
        }
        return false;

    }

    @Override
    public boolean remove(@NotNull final String id, @NotNull final String userId) {
        @Nullable final Task thisTask = Objects.requireNonNull(taskMap).get(id);
        if (thisTask != null) {
            assert thisTask.getUserId() != null;
            if (thisTask.getUserId().equals(userId)) {
                taskMap.remove(id);
                return true;
            }
        }
        return false;
    }

    @Override
    @Nullable
    public HashMap<String, Task> getTaskMap() {
        return (HashMap<String, Task>) taskMap;
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        return Objects.requireNonNull(taskMap).values().stream().filter(task -> Objects.requireNonNull(task.getUserId()).equals(userId)).collect(Collectors.toList());
    }

    @Override
    public boolean removeAll(@NotNull final String userId) {
        Objects.requireNonNull(taskMap).values().stream().filter(task -> Objects.requireNonNull(task.getUserId())
                .equals(userId)).forEach(x -> taskMap.remove(x.getId()));
        return true;
    }
}


