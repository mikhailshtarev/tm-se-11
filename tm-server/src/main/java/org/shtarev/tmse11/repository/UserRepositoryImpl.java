package org.shtarev.tmse11.repository;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.BD.Const;
import org.shtarev.tmse11.commands.UserRole;
import org.shtarev.tmse11.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;


public class UserRepositoryImpl implements UserRepository<User> {
    protected String dbHost = "localhost";
    protected String dbPort = "3306";
    protected String dbUser = "root";
    protected String dbPass = "foxfox";
    protected String dbName = "projecttaskbd";

    Connection dbConnection;

    public Connection getDbConnection()
            throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://+" + dbHost +
                ":" + dbPort + "/" + dbName;
        dbConnection = DriverManager
                .getConnection(connectionString, dbUser, dbPass);
        return dbConnection;
    }

    public void signUpUser(@NotNull final String userId, @NotNull final String name, @NotNull final String password,
                           @NotNull final UserRole[] userRole) {

        String insert = "INSERT INTO" + Const.USER_TABLE + "(" +
                Const.USER_ID + "," + Const.USER_NAME + ","
                + Const.USER_PASSWORD + "," + Const.USER_ROLE + ")" +
                "VALUES(?,?,?,?)";
        try {
            PreparedStatement prST = getDbConnection().prepareStatement(insert);
            prST.setString(1, userId);
            prST.setString(2, name);
            prST.setString(3, password);
            prST.setString(4, Arrays.toString(Arrays.stream(userRole).toArray()));

            prST.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean create(@NotNull final User thisUser) {
        if (thisUser.getName() != null) {
            if (thisUser.getPassword() != null) {
                if (thisUser.getUserRole() != null) {
                    signUpUser(thisUser.getUserId(), thisUser.getName(),
                            thisUser.getPassword(), thisUser.getUserRole().toArray(new UserRole[0]));
                }
            }
        }
        return true;
    }

//    @Override
//    public User getUser(@NotNull final String userName) {
//        return userMap.get(userName);
//    }
//
//    @Override
//    @Nullable
//    public List<User> getUserList() {
//        return new ArrayList<>(userMap.values());
//    }
//
//
//    @Override
//    public boolean rePassword(@NotNull final String userName, @NotNull final String oldPasswordHash,
//                              @NotNull final String newPassword) {
//        @NotNull final User thisUser = userMap.get(userName);
//        if (thisUser != null) {
//            @NotNull final String newPasswordHash = DigestUtils.md5Hex(newPassword);
//            if (thisUser.getPassword().equals(oldPasswordHash))
//                thisUser.setPassword(newPasswordHash);
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public boolean updateUser(@NotNull final String name, @NotNull final String thisUserId) {
//        @NotNull final User thisUser = userMap.get(thisUserId);
//        if (thisUser != null) {
//            thisUser.setName(name);
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public List<UserRole> getListUserRole(String userName) {
//        return userMap.get(userName).getUserRole();
//
//    }
}
