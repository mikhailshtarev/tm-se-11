package org.shtarev.tmse11.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.entity.Session;

import java.util.HashMap;
import java.util.Map;

public class SessionRepositoryImpl implements SessionRepository {

    @Getter
    @NotNull
    private final Map<String, Session> sessionMap = new HashMap<>();

    @Override
    public Session checkSession(@NotNull final String sessionId) {
        return sessionMap.get(sessionId);
    }

    @Override
    public boolean createSes(@NotNull final Session session) {
        if (sessionMap.get(session.getId()) != null) return false;
        sessionMap.put(session.getId(), session);
        return true;
    }

    @Override
    public boolean deleteSession(@NotNull final String sessionId) {
        sessionMap.remove(sessionId);
        return true;
    }
}
