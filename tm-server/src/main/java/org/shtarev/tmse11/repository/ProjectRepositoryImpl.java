package org.shtarev.tmse11.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse11.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepositoryImpl implements ProjectRepository<Project> {

    @NotNull
    private final HashMap<String, Project> projectMap = new HashMap<>();

    @Override
    public boolean create(@NotNull final Project thisProject) {
        if (!projectMap.containsKey(thisProject.getId())) {
            projectMap.put(thisProject.getId(), thisProject);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(@NotNull final String projectId, @NotNull final Project thisProject) {
        if (projectMap.get(projectId) != null) {
            Project project = projectMap.get(projectId);
            project.setDescription(thisProject.getDescription());
            project.setDataStart(thisProject.getDataStart());
            project.setDataFinish(thisProject.getDataFinish());
            project.setTaskProjectStatus(thisProject.getTaskProjectStatus());
            return true;
        }
        return false;
    }


    @Override
    public boolean remove(@NotNull final String Id, @NotNull final String userID) {
        @NotNull final Project thisProject = projectMap.get(Id);
        assert thisProject.getUserId() != null;
        if (thisProject.getUserId().equals(userID)) {
            projectMap.remove(Id);
            return true;
        }
        return false;
    }

    @Override
    public List<Project> getProjectMap(@NotNull final  String userId) {
        @Nullable final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        return projectListValue.stream().filter(project -> project.getUserId().equals(userId)).collect(Collectors.toList());
    }


    @Override
    public boolean removeAll(@NotNull final String userID) {
        @Nullable final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        @Nullable final List<Project> project2List = projectListValue.stream().filter(project -> project.getUserId()
                .equals(userID)).collect(Collectors.toList());
        for (Project thisProject : project2List) {
            projectMap.remove(thisProject.getId());
            return true;
        }
        return false;
    }

}

