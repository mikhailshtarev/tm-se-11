package org.shtarev.tmse11.repository;

import org.shtarev.tmse11.entity.Task;

import java.util.HashMap;
import java.util.List;

public interface TaskRepository<T extends Task> {

    boolean create(final T thisTask);

    boolean update(final String taskId, final T thisProject);

    boolean remove(final String id, final String userID);

    List<T> findAll(final String userID);

    HashMap<String, T> getTaskMap();

    boolean removeAll(final String userID);
}
