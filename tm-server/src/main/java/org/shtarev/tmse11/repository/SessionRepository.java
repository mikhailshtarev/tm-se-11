package org.shtarev.tmse11.repository;

import org.shtarev.tmse11.entity.Session;

public interface SessionRepository {

    Session checkSession(String sessionId);

    boolean createSes(Session session);

    boolean deleteSession(String sessionId);
}
