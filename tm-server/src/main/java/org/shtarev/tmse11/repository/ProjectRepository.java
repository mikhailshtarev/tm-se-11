package org.shtarev.tmse11.repository;

import org.shtarev.tmse11.entity.Project;

import java.util.HashMap;
import java.util.List;

public interface ProjectRepository<T extends Project> {

    boolean create( T thisProject);

    boolean update( String projectId,  T thisProject);

    boolean remove( String Id,  String userID);

    List<Project> getProjectMap(String userId);

    boolean removeAll( String userID);
}