package org.shtarev.tmse11.entity;

import lombok.Getter;
import lombok.Setter;
import org.shtarev.tmse11.util.LocalDateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.UUID;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Getter
@Setter
public class Session {

    private String id = UUID.randomUUID().toString();

    private String Name;

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
    private final LocalDate dataCreate = LocalDate.now();

    private String hashBox;
}
