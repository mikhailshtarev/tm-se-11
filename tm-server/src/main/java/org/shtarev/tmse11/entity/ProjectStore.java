package org.shtarev.tmse11.entity;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "projectStore")
public class ProjectStore {

    private String name;

    private List<Project> listProject;

}
