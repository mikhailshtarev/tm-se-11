package org.shtarev.tmse11.entity;

import java.util.List;


public class UserStore {


    List<User> userList;
    private String name;

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
