package org.shtarev.tmse11.util;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class SignatureUtil {
    public static String sign(@Nullable final Object value,@Nullable final String salt,@Nullable final Integer cycle) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign (json,salt,cycle);
        } catch (final JsonProcessingException ignored) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Nullable
    private static String sign(@Nullable final String value,
                              @Nullable final String salt,
                              @Nullable final Integer cycle) {
        if (value == null || salt == null || cycle == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = Md5.md5(salt + result + salt);
        }
        return result;
    }
}

