package org.shtarev.tmse11.util;

import com.sun.istack.Nullable;
import com.sun.istack.internal.NotNull;
import lombok.Getter;
import lombok.Setter;
import scala.Int;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;



public class HashProperties {

    public static String getSalt(){
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try {
            fileInputStream = new FileInputStream("C:\\projects\\tm-se-11\\tm-server\\src\\main\\resources\\hash.properties");
            prop.load(fileInputStream);
            String salo = prop.getProperty("salo");
            return salo;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Integer getCycol(){
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try {
            fileInputStream = new FileInputStream("C:\\projects\\tm-se-11\\tm-server\\src\\main\\resources\\hash.properties");
            prop.load(fileInputStream);
            Integer cycol = Integer.valueOf(prop.getProperty("cicol"));
            return cycol;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
