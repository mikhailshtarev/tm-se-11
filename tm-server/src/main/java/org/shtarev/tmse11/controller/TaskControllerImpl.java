//package org.shtarev.tmse11.controller;
//
//import org.jetbrains.annotations.NotNull;
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.entity.Task;
//import org.shtarev.tmse11.service.TaskService;
//
//import javax.jws.WebService;
//import java.util.List;
//
//@WebService(endpointInterface = "org.shtarev.tmse11.controller.TaskController")
//
//public class TaskControllerImpl implements TaskController {
//
//    @NotNull
//    private final TaskService<Task> taskService;
//
//    public TaskControllerImpl(@NotNull final TaskService<Task> taskService) {
//        this.taskService = taskService;
//    }
//
//    @Override
//    public boolean taskCreate(@NotNull final Task task, @NotNull final Session session) {
//        return taskService.create(task, session);
//    }
//
//    @Override
//    public boolean taskUpdate(@NotNull final String taskId, @NotNull final Task task,
//                           @NotNull final String taskPS, @NotNull final Session session) {
//        return  taskService.update(taskId, task, taskPS, session);
//    }
//
//    @Override
//    public List<Task> taskFindByDescription(@NotNull final String partName, @NotNull final Session session) {
//        return taskService.findByDescription(partName, session);
//    }
//
//    @Override
//    public boolean taskDelete(@NotNull final String id, @NotNull final Session session) {
//        return  taskService.delete(id, session);
//    }
//
//    @Override
//    public List<Task> taskReadAll(@NotNull final Session session) {
//        return taskService.taskReadAll(session);
//    }
//
//    @Override
//    public boolean taskDeleteAll(@NotNull final Session session) {
//        return taskService.taskDeleteAll(session);
//    }
//}
