package org.shtarev.tmse11.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.commands.UserRole;
import org.shtarev.tmse11.entity.Session;
import org.shtarev.tmse11.entity.User;
import org.shtarev.tmse11.service.UserService;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "org.shtarev.tmse11.controller.UserController")
public class UserControllerImpl implements UserController {

    @NotNull
    private final UserService<User> userService;

    public UserControllerImpl(@NotNull final UserService<User> userService) {
        this.userService = userService;
    }

    @Override
    public boolean userCreate(@NotNull final User user) {
        List<UserRole> listUR = new ArrayList<>(Collections.singleton(UserRole.REGULAR_USER));
        user.setUserRole(listUR);
        return  userService.create(user);
    }

//    @Override
//    public List<User> getUserList(@NotNull final Session session) {
//        return userService.getUserList(session);
//    }
//
//    @Override
//    public Session userLogIn(@NotNull final String name, @NotNull final String password) throws Exception {
//        return userService.LogIn(name, password);
//    }
//
//    @Override
//    public boolean userRePassword(@NotNull final String name, @NotNull final String oldPassword,
//                               @NotNull final String newPassword, @NotNull final Session session) {
//        return  userService.rePassword(name, oldPassword, newPassword, session);
//    }
//
//    @Override
//    public boolean userUpdate(@NotNull final String name, @NotNull final Session session) {
//        return userService.userUpdate(name, session);
//    }
//
//    @Override
//    public List<UserRole> getUserRole(@NotNull final Session session) {
//        return userService.getListUserRole(session);
//    }
}
