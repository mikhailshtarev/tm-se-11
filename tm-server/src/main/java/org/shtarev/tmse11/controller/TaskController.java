//package org.shtarev.tmse11.controller;
//
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.entity.Task;
//
//import javax.jws.WebMethod;
//import javax.jws.WebService;
//import java.util.List;
//
//@WebService
//public interface TaskController {
//
//    @WebMethod
//    boolean taskCreate(Task task, Session session);
//
//    @WebMethod
//    boolean taskUpdate(String taskId, Task task, String taskPS, Session session);
//
//    @WebMethod
//    List<Task> taskFindByDescription(String partName, Session session);
//
//    @WebMethod
//    boolean taskDelete(String id, Session session);
//
//    @WebMethod
//    List<Task> taskReadAll(Session session);
//
//    @WebMethod
//    boolean taskDeleteAll(Session session);
//}
