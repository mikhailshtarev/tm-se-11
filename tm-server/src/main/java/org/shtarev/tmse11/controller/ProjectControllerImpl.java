//package org.shtarev.tmse11.controller;
//
//import org.jetbrains.annotations.NotNull;
//import org.shtarev.tmse11.entity.Project;
//import org.shtarev.tmse11.entity.Session;
//import org.shtarev.tmse11.service.ProjectService;
//
//import javax.jws.WebService;
//import java.util.List;
//import java.util.Set;
//
//@WebService(endpointInterface = "org.shtarev.tmse11.controller.ProjectController")
//public class ProjectControllerImpl implements ProjectController {
//
//
//    @NotNull
//    private final ProjectService<Project> projectService;
//
//    public ProjectControllerImpl(@NotNull final ProjectService<Project> projectService) {
//        this.projectService = projectService;
//    }
//
//    @Override
//    public boolean projectCreate(@NotNull final Project project, @NotNull final Session session) {
//        return projectService.create(project, session);
//    }
//
//    @Override
//    public boolean projectUpdate(@NotNull final String projectId, @NotNull final Project project,
//                              @NotNull final String taskProjectStatus, @NotNull final Session session) {
//        return projectService.update(projectId, project, taskProjectStatus, session);
//    }
//
//    @Override
//    public Set<Project> projectSorted(@NotNull final String sort, @NotNull final Session session) {
//        return projectService.sorted(sort, session);
//    }
//
//    @Override
//    public List<Project> projectFindByDescription(@NotNull final String partName, @NotNull final Session session) {
//        return projectService.findByDescription(partName, session);
//    }
//
//    @Override
//    public boolean projectDelete(@NotNull final String projectId, @NotNull final Session session) {
//        return projectService.delete(projectId, session);
//    }
//
//    @Override
//    public List<Project> projectsReadAll(@NotNull final Session session) {
//        return projectService.projectsReadAll(session);
//    }
//
//    @Override
//    public boolean projectDeleteAll(@NotNull final Session session) {
//        return projectService.projectDeleteAll(session);
//    }
//}
