//package org.shtarev.tmse11.controller;
//
//import org.shtarev.tmse11.entity.Project;
//import org.shtarev.tmse11.entity.Session;
//
//import javax.jws.WebMethod;
//import javax.jws.WebService;
//import java.util.List;
//import java.util.Set;
//
//@WebService
//public interface ProjectController {
//
//
//    @WebMethod
//    boolean projectCreate(Project project, Session session);
//
//    @WebMethod
//    boolean projectUpdate(String projectId, Project project,String taskProjectStatus, Session session);
//
//
//    @WebMethod
//    Set<Project> projectSorted(String sort, Session session);
//
//    @WebMethod
//    List<Project> projectFindByDescription(String partName, Session session);
//
//    @WebMethod
//    boolean projectDelete(String id, Session session);
//
//    @WebMethod
//    List<Project> projectsReadAll(Session session);
//
//    @WebMethod
//    boolean projectDeleteAll(Session session);
//}
