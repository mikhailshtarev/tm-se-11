package org.shtarev.tmse11.controller;

import org.shtarev.tmse11.commands.UserRole;
import org.shtarev.tmse11.entity.Session;
import org.shtarev.tmse11.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface UserController {

    @WebMethod
    boolean userCreate(User user);

//    @WebMethod
//    List<User> getUserList(Session session);
//
//    @WebMethod
//    Session userLogIn(String name, String password) throws Exception;
//
//    @WebMethod
//    boolean userRePassword(String name, String oldPassword, String newPassword, Session session);
//
//    @WebMethod
//    boolean userUpdate(String name, Session session);
//
//    @WebMethod
//    List<UserRole> getUserRole(Session session);
}
