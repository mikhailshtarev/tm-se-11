package org.shtarev.tmse11.commands;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum TaskProjectStatus {
    @XmlEnumValue("PLANNED") PLANNED,
    @XmlEnumValue("IN_THE_PROCESS") IN_THE_PROCESS,
    @XmlEnumValue("READY") READY
}
