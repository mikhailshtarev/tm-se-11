package org.shtarev.tmse11;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse11.commands.UserRole;
import org.shtarev.tmse11.controller.UserControllerImpl;
import org.shtarev.tmse11.entity.Project;
import org.shtarev.tmse11.entity.Task;
import org.shtarev.tmse11.entity.User;
import org.shtarev.tmse11.repository.*;
import org.shtarev.tmse11.service.SessionUtil;
import org.shtarev.tmse11.service.SessionUtilImpl;
import org.shtarev.tmse11.service.UserService;
import org.shtarev.tmse11.service.UserServiceImpl;

import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppPTManager {
    @NotNull
    private final UserRepository<User> userRepository = new UserRepositoryImpl();

    @NotNull
    private final SessionRepository sessionRepository = new SessionRepositoryImpl();
    @NotNull
    private final TaskRepository<Task> taskRepository = new TaskRepositoryImpl();
    @NotNull
    private final ProjectRepository<Project> projectRepository = new ProjectRepositoryImpl();
    @NotNull
    private final SessionUtil sessionUtil = new SessionUtilImpl(sessionRepository);
    //    @NotNull
//    private final ProjectService<Project> projectService = new ProjectServiceImpl(projectRepository, taskRepository, userRepository, sessionUtil);
//    @NotNull
    private final UserService<User> userService = new UserServiceImpl(userRepository, sessionUtil);
//    @NotNull
//    private final TaskService<Task> taskService = new TaskServiceImpl(taskRepository, userRepository, sessionUtil);

    @SneakyThrows
    public static void main(String[] args) {
        AppPTManager appPTManager = new AppPTManager();
        appPTManager.createTwoUser();
        System.setProperty("javax.xml.transform.TransformerFactory", "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
//        Endpoint.publish("http://0.0.0.0:8080/ProjectEndopoint", new ProjectControllerImpl(appPTManager.projectService));
//        Endpoint.publish("http://0.0.0.0:8080/TaskEndopoint", new TaskControllerImpl(appPTManager.taskService));
        Endpoint.publish("http://0.0.0.0:8080/UserEndopoint", new UserControllerImpl(appPTManager.userService));
    }

    public void createTwoUser() {
        List<UserRole> listUR = new ArrayList<UserRole>(Collections.singleton(UserRole.REGULAR_USER));
        List<UserRole> listUR2 = new ArrayList<UserRole>(Collections.singleton(UserRole.ADMIN));
        User user1 = new User();
        User user2 = new User();
        user1.setName("ad");
        user1.setPassword("ad");
        user1.setUserRole(listUR2);
        user2.setName("ru");
        user2.setPassword("ru");
        user2.setUserRole(listUR);
        userService.create(user1);
        userService.create(user2);
    }
}


